# Vozão Digital - WebView Js API

## Descrição Geral

Para utilizar a API é necessário que site do parceiro esteja executando dentro de um WebView do App Vozão Digital.

Para obter acesso ao APP, solicitar ao responsável (adminitrador de TI do Ceará Sport Club). Encaminhar também a URL do site que será carregado dentro do WebView para que o mesmo possa ser configurado no APP.

## Inclusão do código da API Js

``<script src="https://cdn-sd.tecfun.com.br/dev/vd_api_0.0.6.js"></script>``


## Verificando se a API está disponível

```
if (window.vozaoDigitalApi) {
  window.vozaoDigitalApi.onReady(async () => {
    // API disponível para uso 
  });
} else {
  // API não disponível 
}
```

**IMPORTANTE:** Qualquer método da API só deve ser utilizado após evento de onReady ``window.vozaoDigitalApi.onReady()``.

## Obtendo dados do usuário logado no App

```
const userData = await window.vozaoDigitalApi.getUser();
```

Caso sucesso, os dados retornados serão:
```
{
  id: string
  name: string
  familyName: string
  cpf: string
  email: string
  avatar: string
  gender: 'm' | 'f' | 'o'
  birthDay: isoDateTime
}
```

## Obtendo token de acesso do usuário logado na Plataforma da 2Morrow

```
const appVozaoDigitalUser2MorrowToken = await window.vozaoDigitalApi.getUser2MorrowToken();
```

Caso sucesso, será retornando uma string que representa o token do usuário logado na Plataforma da 2Morrow.

**IMPORTANTE:** Token obtido atráves desta API nunca deve ser utilizado diretamente para acessar a API da Plataforma 2Morrow. Este token representa o usuário no contexto do App Vozão Digital. Na próxima seção será descrito como deve ser realizado autenticação na Plataforma da 2Morrow utilizando o token de usuário do APP Vozão Digital.

***
# Como Autenticar Usuário na Plataforma da 2Morrow

Qualquer site de parceiro do CearáSC que estiver sendo acessado dentro do APP Vozão Digital (através de WebView) não deve solicitar credências do usuário (email/senha). 

Para permitir autênticação implícita, a API da 2Morrow possui um novo método [/v2/ChangeFanTokenOwner](https://devextapi.2morrow.com.br/swagger/index.html), no qual é possível realizar autenticação utilizando um token já existente do usuário (ao invés de email/senha).

Após obter o token do usuário 2Morrow logado no App Vozão Digital através da chamada ``vozaoDigitalApi.getUser2MorrowToken()``, deve-se fazer uma chamada a API da Plataforma 2Morrow para obter o novo token do usuário no contexto do parceito.

### Ex. obter token do usuário logado no App Vozão Digital:

```
const appVozaoDigitalUser2MorrowToken = await window.vozaoDigitalApi.getUser2MorrowToken();
```

Após obter o appVozaoDigitalUser2MorrowToken, o mesmo deve ser enviado em uma conexão segura HTTPS (POST) para servidor realizar autenticação na 2Morrow.

> Sugestão de implementação para parte do servidor é adicionar um método adicional de login (específico para o caso do site está rodando dentro do App Vozão Digital e consiga obter token do usuário com sucesso), ou parâmetrizar o atual, para receber um token ao invés de receber usuário/senha.

### Ex. código, ilustrativo, caso hipotético, servidor em NodeJS para autenticar usuário na 2Morrow e obter novo token de acesso do uruário.
```
await response = fetch("https://devextapi.2morrow.com.br/v2/ChangeFanTokenOwner", {
  method: 'POST',
  headers: {"Content-Type", "application/json"},
  body: JSON.stringify({
	  "fanToken": appVozaoDigitalUser2MorrowToken,
	  "newAppToken": "[PartnerToken]"
	})
});
const newUserToken = await response.json();
```

